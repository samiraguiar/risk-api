import typing
import logging
from datetime import datetime as dt
from enum import Enum
# We use `partial` just to be able to use function as enum members,
# so keep the import name short to avoid cluttering the code
from functools import partial as p_

from api.models.user import User, MaritalStatus
from api.models.insurance_line import InsuranceLine
from api.models.risk_score import RiskScore
from api.models.house import HouseOwnershipStatus


#: get a logger for this module only
logger = logging.getLogger(__file__)


class RiskCalculationEnum(Enum):
    """
    Base class for calculation enums.

    .. note:: In enums inheriting from this class we use lambdas wrapped up in
    partials to prevent them from being turned into methods and instead kept as
    enum values.

    However, this has a side effect that these are not callable by default, and
    this base class solves the problem by overloading the __call__ dunder
    method. Additionally, we have lambdas returning lambdas when the filters
    accept parameters, which makes logging difficult as such lambdas keep no
    reference to the enum member that returned them. We work around this by
    setting the `name` attribute on the returned object if it is a lambda.

    This could be done by using metaclasses, but it would require us to rely on
    internal behavior of the Enum module, which is complex enough to disencourage
    this.
    """

    def __call__(self, *args):
        """Make sure members are callable and that we can reference the enum name."""
        ret = self.value(*args)
        # In some cases we have nested lambdas, so make sure
        # we can refer back to the enum name for logging.
        if callable(ret) and ret.__name__ == "<lambda>":
            ret.name = self.name
        return ret


class IfUser(RiskCalculationEnum):
    """Enum class with common conditions."""

    # Boolean conditions
    HAS_NO_INCOME       = p_(lambda user: user.income == 0)
    HAS_NO_HOUSE        = p_(lambda user: not user.has_house)
    HAS_NO_VEHICLE      = p_(lambda user: not user.has_vehicle)
    HAS_DEPENDENTS      = p_(lambda user: user.dependents > 0)
    IS_MARRIED          = p_(lambda user: user.marital_status == MaritalStatus.MARRIED)
    HAS_MORTGAGED_HOUSE = p_(lambda user: user.has_house and
                                          user.house.ownership_status == HouseOwnershipStatus.MORTGAGED)

    # Age conditions
    AGE_LESS_THAN       = p_(lambda a: lambda user: user.age < a)
    AGE_GREATER_THAN    = p_(lambda a: lambda user: user.age > a)
    AGE_BETWEEN         = p_(lambda a1, a2: lambda user: user.age in range(a1, a2))

    # Income conditions
    INCOME_GREATER_THAN = p_(lambda i: lambda user: user.income > i)

    # Vehicle conditions
    VEHICLE_AGE_LE      = p_(lambda y: lambda user: user.has_vehicle and
                                                    (dt.now().year - user.vehicle.year) <= y)


class Then(RiskCalculationEnum):
    """Enum class with common outcomes."""

    SET_INELIGIBLE = p_(lambda _: RiskScore.INELIGIBLE)
    ADD            = p_(lambda v: Then._add(v))
    DEDUCT         = p_(lambda v: Then._add(-v))

    @classmethod
    def _add(cls, value):
        return lambda score: score + value


#: rules used to calculate a risk profile
RULES: typing.Dict[InsuranceLine, typing.Tuple[IfUser, Then]] = {
    InsuranceLine.DISABILITY: [
        (IfUser.HAS_NO_INCOME,               Then.SET_INELIGIBLE),
        (IfUser.AGE_GREATER_THAN(60),        Then.SET_INELIGIBLE),
        (IfUser.AGE_LESS_THAN(30),           Then.DEDUCT(2)),
        (IfUser.AGE_BETWEEN(30, 40),         Then.DEDUCT(1)),
        (IfUser.INCOME_GREATER_THAN(200000), Then.DEDUCT(1)),
        (IfUser.HAS_MORTGAGED_HOUSE,         Then.ADD(1)),
        (IfUser.HAS_DEPENDENTS,              Then.ADD(1)),
        (IfUser.IS_MARRIED,                  Then.DEDUCT(1)),
    ],
    InsuranceLine.AUTO: [
        (IfUser.HAS_NO_VEHICLE,              Then.SET_INELIGIBLE),
        (IfUser.AGE_LESS_THAN(30),           Then.DEDUCT(2)),
        (IfUser.AGE_BETWEEN(30, 40),         Then.DEDUCT(1)),
        (IfUser.INCOME_GREATER_THAN(200000), Then.DEDUCT(1)),
        (IfUser.VEHICLE_AGE_LE(5),           Then.ADD(1)),
    ],
    InsuranceLine.HOME: [
        (IfUser.HAS_NO_HOUSE,                Then.SET_INELIGIBLE),
        (IfUser.AGE_LESS_THAN(30),           Then.DEDUCT(2)),
        (IfUser.AGE_BETWEEN(30, 40),         Then.DEDUCT(1)),
        (IfUser.INCOME_GREATER_THAN(200000), Then.DEDUCT(1)),
        (IfUser.HAS_MORTGAGED_HOUSE,         Then.ADD(1)),
    ],
    InsuranceLine.LIFE: [
        (IfUser.AGE_GREATER_THAN(60),        Then.SET_INELIGIBLE),
        (IfUser.AGE_LESS_THAN(30),           Then.DEDUCT(2)),
        (IfUser.AGE_BETWEEN(30, 40),         Then.DEDUCT(1)),
        (IfUser.INCOME_GREATER_THAN(200000), Then.DEDUCT(1)),
        (IfUser.HAS_DEPENDENTS,              Then.ADD(1)),
        (IfUser.IS_MARRIED,                  Then.ADD(1)),
    ]
}


def _int_to_score(value: int) -> RiskScore:
    """
    Map an integer value to a score value.

    :param int value: value to map
    :returns: the corresponding mapped value
    :rtype: :py:class:`api.models.risk_score.RiskScore`
    """
    if value <= 0:
        return RiskScore.ECONOMIC
    if value >= 3:
        return RiskScore.RESPONSIBLE
    else:
        return RiskScore.REGULAR


def calculate_risk(user: User) -> typing.Dict[InsuranceLine, RiskScore]:
    """
    Calculate the user risk score for the different insurance lines.

    :param user: user to calculate scores for
    :returns: a dictionary with the scores for each line
    """
    base_score_value = user.risk_questions.count(True)
    logger.debug(f"Starting with a base score of {base_score_value=}")

    logger.info("Calculating the risk profile for the user")
    risk_profile = {}
    for line, rules in RULES.items():
        score = base_score_value
        for condition, outcome in rules:
            logger.info(f"Testing for condition `{condition.name}`")
            if condition(user):
                score = outcome(score)
                logger.info(f"Condition matched with `{outcome.name}` producing {score}")

            # We have a risk score (most likely ineligibility), stop processing
            # further rules for this line of insurance
            if isinstance(score, RiskScore):
                logger.info(f"Bailing out as we already have a final score")
                break

        final_score = _int_to_score(score) if isinstance(score, int) else score
        risk_profile[line] = final_score.value

    logger.debug(f"Finished calculating scores ({risk_profile=})")
    return risk_profile
