from enum import Enum
from dataclasses import dataclass
from typing import Optional

from marshmallow import Schema, fields, validate, post_load

from api.models.house import House, HouseSchema
from api.models.vehicle import Vehicle, VehicleSchema


class MaritalStatus(Enum):
    """Possible marital status for a user."""
    SINGLE = "single"
    MARRIED = "married"


@dataclass
class User(object):
    """Class representing a user to calculate a score for."""
    age: int
    dependents: int
    income: int
    marital_status: Optional[MaritalStatus]
    risk_questions: list
    house: House
    vehicle: Optional[Vehicle]

    @property
    def has_house(self):
        """Whether this user owns a house."""
        return self.house is not None

    @property
    def has_vehicle(self):
        """Whether this user owns a vehicle."""
        return self.vehicle is not None


class UserSchema(Schema):
    """User schema class for calculating risk profiles."""

    @post_load
    def make_user(self, data, **_):
        """Deserialize into a User instance instead of a dict."""
        return User(**data)

    #: age (an integer equal or greater than 0)
    age = fields.Int(validate=validate.Range(min=0),
                     required=True)

    #: number of dependents (an integer equal or greater than 0)
    dependents = fields.Int(validate=validate.Range(min=0),
                            required=True)

    #: income (an integer equal or greater than 0)
    income = fields.Int(validate=validate.Range(min=0),
                        required=True)

    #: marital status ("single" or "married")
    marital_status = fields.Str(validate=validate.OneOf([e.value for e in MaritalStatus]),
                                required=True)

    #: risk answers (an array with 3 booleans)
    risk_questions = fields.List(fields.Bool(), validate=validate.Length(3),
                                 required=True)

    #: optional house
    house = fields.Nested(HouseSchema, required=False)

    #: optional vehicle
    vehicle = fields.Nested(VehicleSchema, required=False)
