from dataclasses import dataclass
from marshmallow import Schema, fields, validate, post_load


@dataclass
class Vehicle(object):
    year: int


class VehicleSchema(Schema):
    """Vehicle schema."""

    @post_load
    def make_vehicle(self, data, **_):
        return Vehicle(**data)

    year = fields.Int(validate=validate.Range(min=1800))
