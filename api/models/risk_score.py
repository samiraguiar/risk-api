from enum import Enum


# NOTE: inherit from str to make it serializable
class RiskScore(str, Enum):
    """Enum with possible score values for insurance lines."""

    INELIGIBLE = "ineligible"
    ECONOMIC = "economic"
    RESPONSIBLE = "responsible"
    REGULAR = "regular"
