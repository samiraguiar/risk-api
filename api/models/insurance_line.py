from enum import Enum

# NOTE: inherit from str to make it serializable
class InsuranceLine(str, Enum):
    """Enum with possible insurance lines."""

    AUTO = "auto"
    DISABILITY = "disability"
    HOME = "home"
    LIFE = "life"
