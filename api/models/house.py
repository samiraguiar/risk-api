from enum import Enum
from dataclasses import dataclass

from marshmallow import Schema, fields, validate, post_load


class HouseOwnershipStatus(Enum):
    """Possible ownership status for a house."""
    OWNED = "owned"
    MORTGAGED = "mortgaged"


@dataclass
class House(object):
    """House class representing ownership."""
    ownership_status: HouseOwnershipStatus


class HouseSchema(Schema):
    """House schema to validate ownership status."""

    @post_load
    def make_house(self, data, **_):
        """Deserialize into a House instance instead of a dict."""
        return House(**data)

    ownership_status = fields.Str(validate=validate.OneOf([e.value for e in HouseOwnershipStatus]),
                                  required=True)
