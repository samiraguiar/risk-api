import logging

from flask import Flask, jsonify, request
from flask_jwt import JWT, jwt_required

from api.models.user import UserSchema
from api.models.api_user import ApiUser
from api.controllers import risk_controller as ctrl


#: get a logger for this module only
logger = logging.getLogger(__file__)


#: users allowed to log in (ideally this would be in a database)
dummy_users = [
    ApiUser(1, 'origin', 'financial'),
    ApiUser(2, 'admin', 'nimda'),
]


def authenticate(username, password):
    """
    Authenticate an user.

    :param str usernam: name of the user to authenticate
    :param str password: password of the user
    :returns: a valid user or None if authentication failed
    :rtype: :py:class:`ApiUser`
    """
    user = next((u for u in dummy_users if u.username == username), None)
    if user and user.password == password:
        logger.debug(f"Authenticated {user.username=} with {user.password=}")
        return user
    return None


def identity(payload):
    user_id = payload['identity']
    return next((u for u in dummy_users if u.id == user_id), None)


api = Flask(__name__)
api.config['SECRET_KEY'] = 'super-secret'
jwt = JWT(api, authenticate, identity)


@api.route('/risk', methods=['POST'])
@jwt_required()
def calculate_risk():
    user = UserSchema().load(request.get_json())
    risk_profile = ctrl.calculate_risk(user)
    return jsonify(risk_profile)


if __name__ == "__main__":
    api.run()
