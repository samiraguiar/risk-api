#!/bin/bash

# the directory where this file is
readonly misc_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
readonly fenv="${FLASK_ENV:-development}"

FLASK_APP="api/main.py" FLASK_ENV=$fenv env/bin/python -m flask run --no-debugger
