#!/bin/bash

# the directory where this file is
readonly misc_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

env/bin/python -m unittest discover
