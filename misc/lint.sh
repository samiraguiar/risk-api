#!/bin/bash

# the directory where this file is
readonly misc_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

cd "${misc_dir}/.." || exit 1

echo ">> Running pylint"
env/bin/python -m pylint api

echo
echo ">> Running pycodestyle"
env/bin/python -m pycodestyle api
