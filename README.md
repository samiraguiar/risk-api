## Preparing

First, create a virtual environment and install dependencies (the name of the environment directory matters):

```
virtualenv env
env/bin/python -m pip install -r requirements.txt
```

## Running

- To serve the API, launch `misc/run.sh`

- Then on a separate shell, acquire a token:

    ```bash
        TOKEN=$(curl -s -X POST -H "Content-Type: application/json" -d '{
            "username": "origin",
            "password": "financial"
        }' http://localhost:5000/auth | grep "access_token" | awk -F\" '{ print $4 }')
    ```

- You can then issue new requests by adding this token to the headers:

    ```bash
    curl -X POST -H "Content-Type: application/json" -H "Authorization: JWT ${TOKEN}" -d '{
        "age": 35,
        "dependents": 2,
        "house": {"ownership_status": "owned"},
        "income": 0,
        "marital_status": "married",
        "risk_questions": [0, 1, 0],
        "vehicle": {"year": 2018}
    }' http://localhost:5000/risk
    ```

## Decisions

#### Virtual environment
During development I used virtualenv to keep dependencies local.

#### Web Framework

This was my first real contact with Python web frameworks since my experience with it has been somewhat restricted to desktop/server applications only.

My original idea was to use Django with the Django REST framework, but that turned out to be too complex for this simple exercise. It envolved dealing with Django concepts of databases, ORMs, URL configuration and more, so I decided to try something simpler. Flask seemed perfect for this sample project, and it was what I ended up choosing.

#### Authentication

I decided to add authentication for completeness, using the first package I could find for Flask. I didn't go as far as adding persistence but rather used hardcoded values.

#### Architecture

The chosen architecture was MVC since it fits the business rules described in the exercise. It also seems to be commonly used in Flask applications.

#### Validation

I decided to use marshmallow since validation and serialization is something we always have to do when developing for the web, and due to the nature of the entities we serialize to that often leads to boilerplate. When learning about Flask I stumbled upon marshmallow and it became my choice for its simplicity and for the fact that it does the two jobs I needed (serialization+validation).

#### Risk profile calculation

The calculation of the risk profile, along with enums and classes needed were kept in the risk controller. The reasoning behind this choice was mostly encapsulation since there was no need to isolate them or make them reusable in external modules as that controller is the only piece of code using them.

#### Typings

I tried to add typings wherever I could, but left them out of schemas on purpose as they would be of little value there.

#### Documentation

I chose sphinx for the documentation generation and documentation format since this is the one I use at work and am most comfortable with.

#### Tests

I usually split tests in Python projects into two directories, "unit" and "integration". Tests of the first types are here focused on smaller components and validating the business rule, while those in the other group are meant to assert that the moving pieces are working together. In this case, my integration tests checked the path from the initial request to the final response.

#### Linting

I often try to use lint tools to detect possible errors in code, and did the same here via the script `misc/lint.sh`. There are some pending issues to fix but they are all cosmetic.

#### Logging

I added some basic logging facilities via the buit-in logging module. At work we use it heavily and it does the job pretty well, so I did not find a reason to opt for something else.
