import unittest
import json

from api.main import api


class TestAuth(unittest.TestCase):
    """Integration tests for the auth route."""

    def setUp(self) -> None:
        self.api = api.test_client()
        return super().setUp()

    def test_success_auth(self):
        """Test that we can successfully login with a user."""
        auth_response = self._login('admin', 'nimda')
        self.assertEqual(auth_response.status_code, 200)
        data = json.loads(auth_response.data)
        self.assertIsNotNone(data["access_token"])

    def test_failed_auth(self):
        """Test that a user cannot login without valid credentials."""
        response = self._login('joe', 'some_password')
        self.assertEqual(response.status_code, 401)

    def _login(self, username, password):
        response = self.api.post('/auth', data=json.dumps({
            'username': username,
            'password': password
        }), content_type='application/json')
        return response


if __name__ == '__main__':
    unittest.main()
