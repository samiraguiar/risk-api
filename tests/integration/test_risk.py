import unittest
import json

from api.main import api


class TestRisk(unittest.TestCase):
    """Integration tests for the risk route."""

    def test_simple_request_all_fields(self):
        """Test that a simple request with all fields returns a correct JSON response."""
        with api.test_client() as risk_api:
            auth_response = risk_api.post('/auth', data=json.dumps({
                'username': 'origin',
                'password': 'financial'
            }), content_type='application/json')

            self.assertEqual(auth_response.status_code, 200)
            token = json.loads(auth_response.data)['access_token']

            response = risk_api.post('/risk', data=json.dumps({
                'age': 35,
                'dependents': 2,
                'house': {'ownership_status': 'owned'},
                'income': 0,
                'marital_status': 'married',
                'risk_questions': [0, 1, 0],
                'vehicle': {'year': 2018}
            }), content_type='application/json', headers={ 'Authorization': f'JWT {token}'})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')
        response_data = json.loads(response.data)
        expected_data = {
            'auto': 'regular',
            'disability': 'ineligible',
            'home': 'economic',
            'life': 'regular'
        }

        self.assertDictEqual(response_data, expected_data)


if __name__ == '__main__':
    unittest.main()
