import unittest
from unittest.mock import Mock

from api.models.user import User, MaritalStatus
from api.models.house import House, HouseOwnershipStatus
from api.models.vehicle import Vehicle
from api.controllers import risk_controller as ctrl


class TestRisk(unittest.TestCase):
    """Unit tests for the risk controller."""

    def test_enum_names_of_conditions(self):
        """Test that enum members that are lambda contain a name."""
        self.assertEqual(ctrl.IfUser.IS_MARRIED.name, "IS_MARRIED")
        self.assertEqual(ctrl.IfUser.AGE_GREATER_THAN(10).name, "AGE_GREATER_THAN")
        self.assertEqual(ctrl.IfUser.INCOME_GREATER_THAN(15).name, "INCOME_GREATER_THAN")
        self.assertEqual(ctrl.IfUser.VEHICLE_AGE_LE(100).name, "VEHICLE_AGE_LE")

    def test_enum_names_of_outcomes(self):
        """Test that enum members that are lambda contain a name."""
        self.assertEqual(ctrl.Then.SET_INELIGIBLE.name, "SET_INELIGIBLE")
        self.assertEqual(ctrl.Then.ADD(10).name, "ADD")
        self.assertEqual(ctrl.Then.DEDUCT(15).name, "DEDUCT")

    def test_condition_matching(self):
        """Test that a condition matches correctly."""
        user_mock = Mock()
        user_mock.age = 8

        condition = ctrl.IfUser.AGE_LESS_THAN(10)
        self.assertTrue(condition(user_mock))

        user_mock.age = 15
        self.assertFalse(condition(user_mock))

    def test_that_an_outcome_changes_the_score(self):
        """Test that we can successfully apply an outcome."""
        outcome = ctrl.Then.ADD(5)
        self.assertEqual(outcome(2), 7)

    def test_that_risk_profile_is_calculated(self):
        """Test that a risk profile is successfully generated."""
        user = User(28, 2, 1500, MaritalStatus.SINGLE, [0, 1, 1],
                    House(HouseOwnershipStatus.MORTGAGED),
                    Vehicle(1980))
        risk_profile = ctrl.calculate_risk(user)
        self.assertDictEqual(risk_profile, {
            "auto": "economic",
            "disability": "regular",
            "home": "regular",
            "life": "regular"
        })

    def test_risk_profile_without_possessions(self):
        """Test that a profile is generated when user has no car or house."""
        user = User(28, 2, 1500, MaritalStatus.SINGLE, [0, 1, 1],
                    None, None)
        risk_profile = ctrl.calculate_risk(user)
        self.assertDictEqual(risk_profile, {
            "auto": "ineligible",
            "disability": "regular",
            "home": "ineligible",
            "life": "regular"
        })


if __name__ == "__main__":
    unittest.main()
